#ifndef JOYCONVERTERPELICANCMD_H
#define JOYCONVERTERPELICANCMD_H

// This node outputs asctec_msgs::CtrlInput messages. This serves to test the AscTec drivers directly
//   (that is the Asctec Autopilot ROS Node).

#include "droneModuleROS.h"

//Input
#include "sensor_msgs/Joy.h"

//Output
#include "asctec_msgs/CtrlInput.h"
#include "communication_definition.h"

class JoyConverterPelicanCmd : public DroneModule
{

    //Publisher
protected:
    ros::Publisher OutputPubl;

    //Subscriber
protected:
    ros::Subscriber InputSubs;
    void inputCallback(const sensor_msgs::Joy::ConstPtr& msg);


    //Constructors and destructors
public:
    JoyConverterPelicanCmd();
    ~JoyConverterPelicanCmd();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};



#endif // JOYCONVERTERPELICANCMD_H
